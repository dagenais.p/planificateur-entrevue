import { LoginComponent } from './components/login/login.component';
import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterviewersComponent } from './components/interviewers/interviewers.component';
import { PlageshorairesComponent } from './components/plageshoraires/plageshoraires.component';
import { RecruteursComponent } from './components/recruteurs/recruteurs.component';
import { ChampsexpertiseComponent } from './components/champsexpertise/champsexpertise.component';
import { CalendrierComponent } from './components/calendrier/calendrier.component';
import { UserService } from './services/user/user.service';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { AcceuilComponent } from './components/acceuil/acceuil.component';



const routes: Routes = [

  { path: 'acceuil',  component: AcceuilComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'calendrier',  component: CalendrierComponent, canActivate: [UserService],  data: { path: 'calendrier'} },
  { path: 'plageshoraires',  component: PlageshorairesComponent, canActivate: [UserService],  data: { path: 'plageshoraires'} },
  { path: 'champsexpertise',  component: ChampsexpertiseComponent, canActivate: [UserService],  data: { path: 'champsexpertise'} },
  { path: 'recruteurs',  component: RecruteursComponent, canActivate: [UserService],  data: { path: 'recruteurs'} },
  { path: 'interviewers',  component: InterviewersComponent, canActivate: [UserService],  data: { path: 'interviewers'} },
  { path: '*', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(private userService: UserService){}
 }
