import {Router} from '@angular/router';
import { inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Subject } from 'rxjs/internal/Subject';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService implements CanActivate{

  permittedPathByRole = new Map<number, string[]>;

  constructor(private router: Router){
    this.permittedPathByRole.set(1, ['calendrier','plageshoraires','champsexpertise','recruteurs','interviewers']);
    this.permittedPathByRole.set(2, ['calendrier','recruteurs']);
    this.permittedPathByRole.set(3, ['calendrier']);
    this.permittedPathByRole.set(4, ['calendrier']);

    this.role$.subscribe((newRole) => {
      if(newRole == 0){
        this.router.navigateByUrl('login')
      } else {
        this.router.navigateByUrl('acceuil')
      }
    })

  }
  // Observable number sources
  private role = new BehaviorSubject<number>(0);
  private loggingError = new BehaviorSubject<boolean>(false);
  // Observable number streams
  role$ = this.role.asObservable();
  loggingError$ = this.loggingError.asObservable();
  // Service message commands
  setLoggedInUserRole(role: number) {
    this.role.next(role);
  }

 /*
  const fakeAuthCall = new Observable((subscriber) => {
    subscriber.next(1);
    subscriber.next(2);
    subscriber.next(3);
    setTimeout(() => {
      subscriber.next(4);
      subscriber.complete();
    }, 1000);
  });
  */
  fakeAuthCall(loginForm: object): Observable<any>{
    console.log(loginForm)
    return new Observable((subscriber) => {
      setTimeout(()=>{
        if(Math.random() > 0.5){
          const err = new Error('Erreur de Connexion')
          subscriber.error(err)
        }else {
          this.setLoggedInUserRole(1);
          subscriber.next(true)
        }
      },2000)
    })
    

  }
 
  canActivate(route: ActivatedRouteSnapshot): boolean  {
    const latestUserLoggedRole = this.role.value

    if(latestUserLoggedRole !=0){
      return this.permittedPathByRole.get(latestUserLoggedRole)?.includes(route.data['path'])? true: false
    }
    return false;
  }

  canUserActivate(path: string){
    const latestUserLoggedRole = this.role.value

    if(latestUserLoggedRole !=0){
      return this.permittedPathByRole.get(latestUserLoggedRole)?.includes(path)? true: false
    }
    return false;
  }

}
