import { RouterModule } from '@angular/router';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatIconModule} from '@angular/material/icon';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import { CalendrierComponent } from './components/calendrier/calendrier.component';
import { PlageshorairesComponent } from './components/plageshoraires/plageshoraires.component';
import { ChampsexpertiseComponent } from './components/champsexpertise/champsexpertise.component';
import { RecruteursComponent } from './components/recruteurs/recruteurs.component';
import { InterviewersComponent } from './components/interviewers/interviewers.component';
import {MatChipsModule} from '@angular/material/chips';
import { MatMenuModule} from '@angular/material/menu';
import { LoginComponent } from './components/login/login.component';
import {MatInputModule} from '@angular/material/input';
import { AcceuilComponent } from './components/acceuil/acceuil.component'
import { ReactiveFormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { CalendarModule, DateAdapter, MOMENT } from 'angular-calendar';
import { SchedulerModule } from 'angular-calendar-scheduler';
import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
import * as moment from "moment";
registerLocaleData(localeIt);


@NgModule({
  declarations: [
    AppComponent,
    CalendrierComponent,
    PlageshorairesComponent,
    ChampsexpertiseComponent,
    RecruteursComponent,
    InterviewersComponent,
    LoginComponent,
    AcceuilComponent
  ],
  imports: [
    RouterModule.forRoot([
      { path: 'acceuil',  component: AcceuilComponent },
      { path: 'login',  component: LoginComponent },
      { path: 'calendrier',  component: CalendrierComponent},
      { path: 'plageshoraires',  component: PlageshorairesComponent},
      { path: 'champsexpertise',  component: ChampsexpertiseComponent},
      { path: 'recruteurs',  component: RecruteursComponent },
      { path: 'interviewers',  component: InterviewersComponent },
      { path: '*', component: PagenotfoundComponent }
  ]),
    MatButtonModule,
    HttpClientModule,
    MatIconModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatChipsModule,
    MatMenuModule,
    MatInputModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    SchedulerModule.forRoot({ locale: 'en', headerDateFormat: 'daysRange', logEnabled: true }),
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'en-US' },
    { provide: MOMENT, useValue: moment }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
