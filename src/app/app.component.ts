import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { Component } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from './services/user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'PEL';
  yourRole: string;
  role = 0;
  userEmail:string;
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, private userService: UserService, private router: Router) {
    this.userEmail = "philippe.dagenais@blabla.ca"
    this.yourRole = "Déconnecté"
    userService.role$.subscribe(
      role => {
        this.role = role;
        switch ( role ) {
          case 1:
              this.yourRole = "Administrateur du système"
              break;
          case 2:
             this.yourRole = "Recruteur Administrateur"
              break;
          case 3:
              this.yourRole = "Recruteur"
              break;
          case 4:
              this.yourRole = "Interviewer technique"
            break;
          default: 
              this.yourRole = "Déconnecté"
            break;
       }
      });
  }

  
  canUserAccessThisRoute(path: string){
    if(this.role != 0){
      return  this.userService.canUserActivate(path)
    } 
    return false;
  }

  loggedInUserHasRole(role: number){
    this.userService.setLoggedInUserRole(role)
  }
}
