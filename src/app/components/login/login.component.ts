import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder} from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  loading = false;
  loggingError = false;

  loginForm = this.fb.group({
    email: ['', {
        validators: [
           Validators.required, 
           Validators.pattern('^[a-zA-Z.-]+@(BLABLA.ca|blabla.ca)$')],
        updateOn: 'blur'
    }],
    password: [
        '', 
        [Validators.required, Validators.minLength(8)]
    ]
 });


  constructor(private fb:FormBuilder, private userService: UserService, private _snackBar: MatSnackBar){
  }


  ngOnInit(){
    this.userService.role$.subscribe(role => {
      if(role != 0){
        this.loading = false;
      }
    })
  }

  login(){
    this.loading = true;
    this.loggingError = false;
    this.userService.fakeAuthCall(this.loginForm.value).subscribe(  
      res => {
        this.loading = false;
        this.loggingError = false;
      },
      err => {
        this.loading = false;
        this.loggingError = true;
        this.openSnackBar()
      },
      () => console.log("a user try logging in")
    )

  }
  openSnackBar() {
    const config: MatSnackBarConfig = {duration:5000}
    this._snackBar.open("Un problème est survenu lors de la connexion, veuillez réessayer", "Fermer",config);
  }

  courrielErrorMessage(){
    return 'Pas un courriel blabla valide'
  }

  passwordErrorMessage(){
    return 'Mot de passe invalide'
  }
}
