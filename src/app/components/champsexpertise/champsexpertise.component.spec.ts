import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampsexpertiseComponent } from './champsexpertise.component';

describe('TroisComponent', () => {
  let component: ChampsexpertiseComponent;
  let fixture: ComponentFixture<ChampsexpertiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChampsexpertiseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChampsexpertiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
