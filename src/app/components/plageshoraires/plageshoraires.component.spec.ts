import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlageshorairesComponent } from './plageshoraires.component';

describe('DeuxComponent', () => {
  let component: PlageshorairesComponent;
  let fixture: ComponentFixture<PlageshorairesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlageshorairesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlageshorairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
